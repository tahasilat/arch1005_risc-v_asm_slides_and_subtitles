1
00:00:00,320 --> 00:00:05,440
so this is the code that we saw for the

2
00:00:02,200 --> 00:00:07,399
unoptimized version of just return and I

3
00:00:05,440 --> 00:00:10,000
want to now make a claim that the stuff

4
00:00:07,399 --> 00:00:13,080
in blue is essentially the setup and

5
00:00:10,000 --> 00:00:14,920
tear down of the frame pointer usage by

6
00:00:13,080 --> 00:00:16,840
this particular code and the stuff in

7
00:00:14,920 --> 00:00:18,320
green is basically stuff we already saw

8
00:00:16,840 --> 00:00:21,000
set the return value return from

9
00:00:18,320 --> 00:00:23,039
function so that's the basics of what we

10
00:00:21,000 --> 00:00:25,439
actually need here and so I'm claiming

11
00:00:23,039 --> 00:00:28,240
the rest of this is extras added for

12
00:00:25,439 --> 00:00:31,160
frame pointer handling but how can I

13
00:00:28,240 --> 00:00:33,440
prove it well I could tell the compiler

14
00:00:31,160 --> 00:00:37,120
to not include frame pointers by using

15
00:00:33,440 --> 00:00:39,960
the- F omit frame pointer argument to

16
00:00:37,120 --> 00:00:42,559
compilation if I do that then I have-

17
00:00:39,960 --> 00:00:44,480
fomit frame pointer down here and that

18
00:00:42,559 --> 00:00:46,199
yields the following code which is

19
00:00:44,480 --> 00:00:48,199
exactly what I said we've got the set

20
00:00:46,199 --> 00:00:49,840
return value and the return from

21
00:00:48,199 --> 00:00:52,520
function

22
00:00:49,840 --> 00:00:54,120
taada but also keep in mind this is

23
00:00:52,520 --> 00:00:56,840
still not actually the same as the

24
00:00:54,120 --> 00:00:58,719
optimized code the optimized didn't have

25
00:00:56,840 --> 00:01:02,120
this dumb little monkey business of

26
00:00:58,719 --> 00:01:05,119
moving 65 to a5 and then a5 to a0 it

27
00:01:02,120 --> 00:01:07,240
just moved 65 to a0 because that's what

28
00:01:05,119 --> 00:01:10,080
optimized code does so what's the

29
00:01:07,240 --> 00:01:12,320
ultimate point of frame pointers well

30
00:01:10,080 --> 00:01:15,280
they are helping to organize the stack

31
00:01:12,320 --> 00:01:17,200
frames into a singly length list it

32
00:01:15,280 --> 00:01:19,479
looks something like this something

33
00:01:17,200 --> 00:01:21,040
calls into main main is going to call

34
00:01:19,479 --> 00:01:23,159
foo foo is going to call bar but

35
00:01:21,040 --> 00:01:25,479
something calls into main and that

36
00:01:23,159 --> 00:01:27,880
causes main to allocate a stack frame

37
00:01:25,479 --> 00:01:30,640
for itself subsequently the frame

38
00:01:27,880 --> 00:01:32,600
pointer is saved by main

39
00:01:30,640 --> 00:01:35,000
and the frame pointer is currently

40
00:01:32,600 --> 00:01:37,439
pointing somewhere up above main and it

41
00:01:35,000 --> 00:01:39,240
will be moved down and we'll see in Risk

42
00:01:37,439 --> 00:01:42,360
5 that it actually looks like it's moved

43
00:01:39,240 --> 00:01:45,000
down to pointing just below the frame

44
00:01:42,360 --> 00:01:47,320
for main itself so you save it you move

45
00:01:45,000 --> 00:01:50,880
down the frame pointer then main calls

46
00:01:47,320 --> 00:01:53,040
fu fu allocates a stack frame for itself

47
00:01:50,880 --> 00:01:55,240
the existing frame pointer value is

48
00:01:53,040 --> 00:01:57,039
stored so that is pointing where the

49
00:01:55,240 --> 00:01:58,840
current frame pointer points and then

50
00:01:57,039 --> 00:02:01,360
the frame pointers move down again

51
00:01:58,840 --> 00:02:03,880
pointing just out side or just to a

52
00:02:01,360 --> 00:02:06,119
higher address or just below from a

53
00:02:03,880 --> 00:02:09,000
stock bottom stack top perspective just

54
00:02:06,119 --> 00:02:11,200
below the foo frame and again call foo

55
00:02:09,000 --> 00:02:13,760
calls to Bar Bar saves off a copy of the

56
00:02:11,200 --> 00:02:15,480
saved frame pointer saved frame pointer

57
00:02:13,760 --> 00:02:17,400
goes down now you'll see that it looks

58
00:02:15,480 --> 00:02:19,280
like the saved frame pointer gets stored

59
00:02:17,400 --> 00:02:21,200
at a slightly different location for

60
00:02:19,280 --> 00:02:22,200
this last function that's called that's

61
00:02:21,200 --> 00:02:24,440
just going to be a little bit of

62
00:02:22,200 --> 00:02:27,360
foreshadowing for later on but anyways

63
00:02:24,440 --> 00:02:29,560
once bar is done then it deallocates its

64
00:02:27,360 --> 00:02:32,280
stack frame by setting the frame pointer

65
00:02:29,560 --> 00:02:34,879
the register the s0 or the frame pointer

66
00:02:32,280 --> 00:02:37,400
register to point at the same location

67
00:02:34,879 --> 00:02:40,239
that the saved one is saying to point to

68
00:02:37,400 --> 00:02:42,720
and then it deallocates its stack frame

69
00:02:40,239 --> 00:02:44,879
then foo returns back to main moves the

70
00:02:42,720 --> 00:02:47,440
frame pointer before it exits and

71
00:02:44,879 --> 00:02:49,640
deallocates its stack frame main returns

72
00:02:47,440 --> 00:02:51,400
back to whoever called it moves the

73
00:02:49,640 --> 00:02:53,640
frame pointer register back to whatever

74
00:02:51,400 --> 00:02:56,000
the saved copy is and deallocates its

75
00:02:53,640 --> 00:02:58,400
own frame pointer so obviously this is

76
00:02:56,000 --> 00:03:01,120
going to matter and make more sense once

77
00:02:58,400 --> 00:03:05,040
we get to functions calling functions

78
00:03:01,120 --> 00:03:05,040
but that'll be later on in the class

