1
00:00:00,719 --> 00:00:05,520
continuing our exploration of compound
variable types we're now going to

2
00:00:05,520 --> 00:00:12,240
introduce structs or structures so I
typed f a new mystruct of type my

3
00:00:12,240 --> 00:00:18,960
structor T which shall contain a single
short six integers all in an array and a

4
00:00:18,960 --> 00:00:25,240
single long long so that structure is
declared my struct foo and then f.a which

5
00:00:25,240 --> 00:00:31,279
is the short is set to babe f. C is set
to Balboa bled blood because it's always

6
00:00:31,279 --> 00:00:37,719
like I say to my wife Babe b blood blood
anyways food. a is Babe which was a

7
00:00:37,719 --> 00:00:42,960
short it is now being assigned to an INT
B of one that's going to lead to some

8
00:00:42,960 --> 00:00:48,440
sign extension because it is a signed
short and a signed int so that 16bit

9
00:00:48,440 --> 00:00:52,879
value is sign extended up to a 32-bit
value then we do math on the 32-bit

10
00:00:52,879 --> 00:00:57,600
Value plus the 64-bit value so again
there's going to be some sign extension

11
00:00:57,600 --> 00:01:04,280
going on and storing it into a smaller
variable the integer so 64-bit Value

12
00:01:04,280 --> 00:01:09,680
plus 32-bit value into a 32-bit storage
and ultimately returning that storage

13
00:01:09,680 --> 00:01:14,840
once again compiled without the stack
protector to simplify the code slightly

14
00:01:14,840 --> 00:01:20,280
so here's what we get and we get a bunch
of new assembly instructions so let's

15
00:01:20,280 --> 00:01:26,560
check them out first we have store
halfword so halfword again 16bit values

16
00:01:26,560 --> 00:01:31,240
that makes sense we had a 16 bit value
in this code that short a so to write

17
00:01:31,240 --> 00:01:36,960
the value babee to a we're going to
store a half word so this is going to be

18
00:01:36,960 --> 00:01:42,439
a store which stores from left to right
we've got a calculation of a memory

19
00:01:42,439 --> 00:01:47,560
address and that is the parentheses
saying memory address and it is rs1

20
00:01:47,560 --> 00:01:52,479
address plus sign extended immediate 12
that is the memory address where we

21
00:01:52,479 --> 00:01:59,119
shall store the halfword sized lower 16
bits from rs2 and again I'm a broken

22
00:01:59,119 --> 00:02:03,960
record here thanks to the miracle of
copy and paste unlike most instructions

23
00:02:03,960 --> 00:02:09,759
stores are read from left to right this
arrow goes that way for everything else

24
00:02:09,759 --> 00:02:14,160
it goes the opposite direction and the
parentheses indicate the parentheses

25
00:02:14,160 --> 00:02:18,800
specifically in the disassembled output
indicate that something is being used as

26
00:02:18,800 --> 00:02:22,879
a memory address and there is going to
be a dereferencing of memory for either

27
00:02:22,879 --> 00:02:26,599
reading or writing on the flip side
there is load half word so we're going

28
00:02:26,599 --> 00:02:33,200
to get something back out from memory so
this is red from right to left so rs1

29
00:02:33,200 --> 00:02:39,239
plus immediate 12 pluck a half word 16
bits out of that and store it into this

30
00:02:39,239 --> 00:02:42,920
register now this is going to sign
extend it to

31
00:02:42,920 --> 00:02:48,920
32bits and if we're in 32-bit code then
you're done but if you are in 64-bit

32
00:02:48,920 --> 00:02:53,840
code then it will further sign extended
up to the full 64-bit size of your

33
00:02:53,840 --> 00:02:58,640
64-bit register broken record once again
this instruction is read from right to

34
00:02:58,640 --> 00:03:03,040
left and because there's parentheses we
know that it is using memory then we

35
00:03:03,040 --> 00:03:09,040
picked up another instruction add W for
add word register to word register so

36
00:03:09,040 --> 00:03:14,280
two parameters a source one and a source
two so add these two together and place

37
00:03:14,280 --> 00:03:20,120
the result in the destination but the
key thing here is this is an RV64I only

38
00:03:20,120 --> 00:03:25,879
assembly instruction so it is dealing
with word registers and normally RV64I

39
00:03:25,879 --> 00:03:29,720
is dealing with double word sized
registers they're 64 bits but this is

40
00:03:29,720 --> 00:03:34,080
instruction is saying specifically I'm
in 64-bit mode but I want to deal in

41
00:03:34,080 --> 00:03:40,360
32-bit registers so consequently it's
going to take the bottom 32 bits of rs1

42
00:03:40,360 --> 00:03:47,120
and the bottom 32 bits of rs2 and add
them together yielding a 32-bit result

43
00:03:47,120 --> 00:03:51,280
and then it will sign extend that 64
bits and store the result into the

44
00:03:51,280 --> 00:03:54,519
destination register and this
instruction isn't going to care if there

45
00:03:54,519 --> 00:03:58,519
was some sort of overflow it's just
going to add them together and there's

46
00:03:58,519 --> 00:04:02,000
no indication anywhere that the result
may have overflown 

47
00:04:02,000 --> 00:04:04,170
and then there's the oh-so-conspicuous

48
00:04:04,170 --> 00:04:11,331
sext instruction so SEXT.W
specifically is sign extend word in

49
00:04:11,331 --> 00:04:17,651
register to double word register so this
is an RV64I instruction and it is

50
00:04:17,651 --> 00:04:23,171
saying Hey I want to treat rs1 as a
32-bit word register and I want that

51
00:04:23,171 --> 00:04:28,970
sign extended and just placed into the
dword double word register destination

52
00:04:28,970 --> 00:04:34,731
but this is scandalous isn't it RISC
five encouraging sexting what about the

53
00:04:34,731 --> 00:04:39,970
children won't somebody please think of
the children well it's just that UNH

54
00:04:39,970 --> 00:04:43,771
wholesome influence of those pseudo
instructions at it again and so as a

55
00:04:43,771 --> 00:04:48,250
reminder I've never actually seen this
vtv show so I have no context for what

56
00:04:48,250 --> 00:04:51,611
this picture is but I can tell you it's
extra inappropriate when we add that

57
00:04:51,611 --> 00:04:56,091
little jiggle so let's go ahead and get
rid of that UNH wholesome pseudo

58
00:04:56,091 --> 00:05:00,891
instruction through the use of chaos
magic rewrite reality and there we go

59
00:05:00,891 --> 00:05:06,611
nice clean assembly traditional Family
Values no more Sexting in our assembly

60
00:05:06,611 --> 00:05:12,171
but we do pick up some compressed Lou
some compressed ad IW which we haven't

61
00:05:12,171 --> 00:05:17,051
seen ad IW yet oh wait there it is AD IW
down a couple assembly instructions

62
00:05:17,051 --> 00:05:21,291
later and a compressed at w we just
literally learned about ad W and now

63
00:05:21,291 --> 00:05:25,051
we're picking up the compressed form
great hope you remember it well anyway

64
00:05:25,051 --> 00:05:31,451
the sexting is not sexting it's a pseudo
instruction which is actually ad IW and

65
00:05:31,451 --> 00:05:38,250
this zero for the immediate so what is
AD IW ad IW is ADD immediate to word

66
00:05:38,250 --> 00:05:42,891
it's another one of these RV64I
instructions where the W suffix is

67
00:05:42,891 --> 00:05:47,811
specifically telling us we want to deal
in word-sized registers so we've got a

68
00:05:47,811 --> 00:05:51,410
source we've got an immediate we've got
a destination so this is going to take

69
00:05:51,410 --> 00:05:58,011
the bottom 32bits word size of the rs1
source register and then take the 12-bit

70
00:05:58,011 --> 00:06:03,130
immediate and sign extended to only 32
bits add them together yield a 32bit

71
00:06:03,130 --> 00:06:07,051
result and then sign extend that
ultimately and place it into the

72
00:06:07,051 --> 00:06:11,491
destination register because we're still
in RV 64 mode and it is a 64-bit

73
00:06:11,491 --> 00:06:18,091
register so this again like the ad W is
going to ignore any sort of overflow

74
00:06:18,091 --> 00:06:21,731
that occurs as a result of this addition
but the key thing is that we can see

75
00:06:21,731 --> 00:06:25,371
that if SE which behind the scenes is
really an Adi

76
00:06:25,371 --> 00:06:30,130
destination source and a zero it's
really just taking advantage of the fact

77
00:06:30,130 --> 00:06:35,011
that the ad IW is performing this sign
extension after the addition and then

78
00:06:35,011 --> 00:06:38,331
it's saying you know what I'm going to
add zero I'm going to just treat this as

79
00:06:38,331 --> 00:06:45,331
a word register 32 bits add zero and
then the adiw will do a 32 to 64-bit

80
00:06:45,331 --> 00:06:50,091
sign extension for me so sexting is
really just adiw when we got rid of the

81
00:06:50,091 --> 00:06:55,491
instruction aliases we did also see a
compressed at IW and this is the same

82
00:06:55,491 --> 00:07:00,371
ultimate result but it is of course
compressed it has less bits and so while

83
00:07:00,371 --> 00:07:04,491
it'll be written like destination
register and an immediate that immediate

84
00:07:04,491 --> 00:07:09,491
is actually only a six-bit immediate and
the destination is both a source and a

85
00:07:09,491 --> 00:07:16,051
destination so basically you're going to
do Rd plus sign extended immediate 6 and

86
00:07:16,051 --> 00:07:21,451
store the result into Rd now there is a
caveat here that Rd may not be the zero

87
00:07:21,451 --> 00:07:25,451
register because that's reserved and
then moving back back to before we

88
00:07:25,451 --> 00:07:29,611
learned about sex we had the add W
instruction we just learned about it

89
00:07:29,611 --> 00:07:34,651
this is now the compressed add word
register to word register so ad IW

90
00:07:34,651 --> 00:07:39,130
versus add W they're all just additions
on word registers one of them is

91
00:07:39,130 --> 00:07:42,731
register to register ad and one of them
is an immediate to register ad this is

92
00:07:42,731 --> 00:07:47,611
the register to register so it is giving
us a destination and a source they are

93
00:07:47,611 --> 00:07:52,771
destination prime source prime that
expands to the uncompressed form of

94
00:07:52,771 --> 00:07:58,530
destination plus source two because this
is also implicitly source one source one

95
00:07:58,530 --> 00:08:02,571
source two add them together place them
into the destination register but this

96
00:08:02,571 --> 00:08:08,690
is an add W so treat those things like
they are 32-bit values so r d prime is

97
00:08:08,690 --> 00:08:14,011
the bottom 32 of whatever the initial Rd
prime value is plus the bottom 32 bits

98
00:08:14,011 --> 00:08:19,891
of rs2 prime get a 32-bit result sign
extended to 64 bits a quick reminder on

99
00:08:19,891 --> 00:08:24,171
LUI from before LUI had a
destination register and a upper IM

100
00:08:24,171 --> 00:08:28,610
mediate to 20 bits and it set bits 31
through 12 so that we could ultimately

101
00:08:28,610 --> 00:08:34,331
create things like 32-bit constants well
there is a compressed LUI that popped

102
00:08:34,331 --> 00:08:40,531
up in this code and the encoding is like
this got a nonzero upper immediate six

103
00:08:40,531 --> 00:08:43,610
and so it gets a little more complicated
because it's not just setting the upper

104
00:08:43,610 --> 00:08:49,451
20 bits it's setting bits 17 through 12
with our six bits there and then it

105
00:08:49,451 --> 00:08:54,771
takes and it s extends bit 17 to the
rest of the upper bits and it Z zeros

106
00:08:54,771 --> 00:08:59,691
out the bottom 12 bits just like the
LUI did before so I think um well let

107
00:08:59,691 --> 00:09:03,331
me just say quick that like the there is
a further caveat that the destination

108
00:09:03,331 --> 00:09:08,490
register cannot be x0 or x2 but I think
this human readable text makes it a

109
00:09:08,490 --> 00:09:13,811
little clear compressed LUI loads the
nonzero 6bit immediate field into bits

110
00:09:13,811 --> 00:09:19,610
17 to 12 of the destination register
clears the bottom 12 bits and sign

111
00:09:19,610 --> 00:09:24,771
extends bit 17 into all higher bits of
the destination all higher bit of the

112
00:09:24,771 --> 00:09:29,490
destination being a nice generic way to
say that although this is an rv32

113
00:09:29,490 --> 00:09:33,691
compressed instruction if it was
executed as it is here in the context of

114
00:09:33,691 --> 00:09:38,290
64-bit code all higher bits would mean
it will sign extend that bit 17 all the

115
00:09:38,290 --> 00:09:43,771
way up to bit 63 the maximum size for
the 64-bit destination register and so

116
00:09:43,771 --> 00:09:46,931
with that we've learned way more
assembly instructions than we want to so

117
00:09:46,931 --> 00:09:51,851
it's time for you once again to go off
and draw a stack diagram figure out how

118
00:09:51,851 --> 00:09:57,091
that my struct T is stor on the stack
what stored where what order is it

119
00:09:57,091 --> 00:10:02,411
reordered by the compiler let's figure
it out by reading the code so go ahead

120
00:10:02,411 --> 00:10:07,971
and step through the code you can see it
with your instruction aliases the sex or

121
00:10:07,971 --> 00:10:12,251
you can see it without the instruction
aliases the ADDI whatever is more

122
00:10:12,251 --> 00:10:16,050
convenient for you

