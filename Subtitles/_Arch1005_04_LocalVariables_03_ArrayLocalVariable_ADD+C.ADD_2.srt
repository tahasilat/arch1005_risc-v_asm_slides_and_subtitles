1
00:00:00,520 --> 00:00:05,000
so if you stepped through main 44 which

2
00:00:03,040 --> 00:00:07,279
is the last place where there's a store

3
00:00:05,000 --> 00:00:09,519
you should have seen a stack that looks

4
00:00:07,279 --> 00:00:10,759
something like this as usual frame

5
00:00:09,519 --> 00:00:12,480
pointer pointing at something from

6
00:00:10,759 --> 00:00:14,360
before main we don't care about saved

7
00:00:12,480 --> 00:00:16,720
frame pointer right after that and we

8
00:00:14,360 --> 00:00:19,439
now know that this extra 8 bytes is

9
00:00:16,720 --> 00:00:21,439
uninitialized data space just for the

10
00:00:19,439 --> 00:00:23,480
alignment keeping things aligned to a

11
00:00:21,439 --> 00:00:25,560
hex1 boundary which is why it went down

12
00:00:23,480 --> 00:00:27,199
to F0 but the key thing you were

13
00:00:25,560 --> 00:00:30,039
supposed to figure out here is what's

14
00:00:27,199 --> 00:00:31,880
going on with the A and the B and so if

15
00:00:30,039 --> 00:00:33,640
you looked carefully about where things

16
00:00:31,880 --> 00:00:36,280
are stored what their particular values

17
00:00:33,640 --> 00:00:40,879
are you would have seen that b is stored

18
00:00:36,280 --> 00:00:45,600
here at F's 8 E8 and you would have seen

19
00:00:40,879 --> 00:00:47,920
that a of one is at 7 FS 8 c0 so that's

20
00:00:45,600 --> 00:00:50,039
the a that's the other random constant

21
00:00:47,920 --> 00:00:53,840
but why was that constant used because

22
00:00:50,039 --> 00:00:56,000
A's plus that gives us bboa bled blood

23
00:00:53,840 --> 00:00:58,519
so the takeaway for this is that it's

24
00:00:56,000 --> 00:01:00,440
the same basic Arrangement as we saw for

25
00:00:58,519 --> 00:01:03,199
scalar local variables

26
00:01:00,440 --> 00:01:06,240
we had B declared second and a declared

27
00:01:03,199 --> 00:01:08,600
first but B got the higher address and a

28
00:01:06,240 --> 00:01:10,479
got the lower address I think we should

29
00:01:08,600 --> 00:01:14,400
throw a little bit of chaos magic at it

30
00:01:10,479 --> 00:01:16,799
and what do we see we see ac. add

31
00:01:14,400 --> 00:01:20,000
compressed add extra new compressed

32
00:01:16,799 --> 00:01:22,680
instruction so how does that operate so

33
00:01:20,000 --> 00:01:24,240
compressed ad register to register is

34
00:01:22,680 --> 00:01:26,680
now actually going to be using that

35
00:01:24,240 --> 00:01:30,200
implicit two form so the destination is

36
00:01:26,680 --> 00:01:32,119
an implicit source source destination

37
00:01:30,200 --> 00:01:34,399
this register whatever it is plus that

38
00:01:32,119 --> 00:01:38,040
register store it back into that

39
00:01:34,399 --> 00:01:41,560
register so for instance x1 plus x2

40
00:01:38,040 --> 00:01:43,960
store it back into x1 now this is unlike

41
00:01:41,560 --> 00:01:45,880
the RV32I where I said specifically

42
00:01:43,960 --> 00:01:47,880
they had to have three and so it looks

43
00:01:45,880 --> 00:01:50,280
like the compressed is fine with the two

44
00:01:47,880 --> 00:01:52,439
register form probably because of the

45
00:01:50,280 --> 00:01:54,759
fact that they have so few bits in which

46
00:01:52,439 --> 00:01:56,920
to encode everything and indeed because

47
00:01:54,759 --> 00:01:59,200
of the fact that they go with this two

48
00:01:56,920 --> 00:02:01,680
register form with the implicit use of

49
00:01:59,200 --> 00:02:03,799
the destination as a source that

50
00:02:01,680 --> 00:02:06,399
actually allows them to still specify

51
00:02:03,799 --> 00:02:09,360
all 32 possible register values for

52
00:02:06,399 --> 00:02:11,800
source and destination modulo a caveat

53
00:02:09,360 --> 00:02:14,879
that basically the rs2 is not allowed to

54
00:02:11,800 --> 00:02:16,360
be x0 because that instruction encoding

55
00:02:14,879 --> 00:02:18,640
is actually encoding some other

56
00:02:16,360 --> 00:02:20,840
instructions and the final takeaway from

57
00:02:18,640 --> 00:02:23,560
this code is that we finally see the use

58
00:02:20,840 --> 00:02:26,239
of CI 16sp up here at the top of the

59
00:02:23,560 --> 00:02:29,360
code I said when we saw it down at the

60
00:02:26,239 --> 00:02:30,840
epilog we'd eventually stop seeing CI at

61
00:02:29,360 --> 00:02:33,720
the beginning because this is actually

62
00:02:30,840 --> 00:02:35,599
the more generic instruction and so it's

63
00:02:33,720 --> 00:02:38,640
perfectly balanced as all things should

64
00:02:35,599 --> 00:02:41,920
be we've got the CI stack pointer minus

65
00:02:38,640 --> 00:02:46,440
80 at the top for the function prologue

66
00:02:41,920 --> 00:02:48,440
CI 16sp plus 80 to restore the stack at

67
00:02:46,440 --> 00:02:51,120
the end in the function epilog so what

68
00:02:48,440 --> 00:02:54,760
do we pick up in this section the new ad

69
00:02:51,120 --> 00:02:57,159
instruction simple ad elegant and add

70
00:02:54,760 --> 00:03:01,760
pops up here to complement our ADDI and

71
00:02:57,159 --> 00:03:01,760
it also gains a compressed ad form

