1
00:00:00,120 --> 00:00:04,359
so here's our first dedicated set

2
00:00:02,399 --> 00:00:07,040
example and we are going to go back to

3
00:00:04,359 --> 00:00:10,040
another control flow we've got if a

4
00:00:07,040 --> 00:00:12,759
greater than zero return one else return

5
00:00:10,040 --> 00:00:15,080
zero so compiling that at optimization

6
00:00:12,759 --> 00:00:17,359
level two gives us this but I'm just

7
00:00:15,080 --> 00:00:20,920
going to tell you that set greater than

8
00:00:17,359 --> 00:00:22,519
zero is a pseudo instruction so set

9
00:00:20,920 --> 00:00:24,480
greater than zero would be the notion of

10
00:00:22,519 --> 00:00:27,000
you have a source and if the source is

11
00:00:24,480 --> 00:00:29,439
greater than zero for per a signed

12
00:00:27,000 --> 00:00:31,720
comparison set the destination to one

13
00:00:29,439 --> 00:00:34,239
and otherwise set the destination is

14
00:00:31,720 --> 00:00:36,120
zero but it's a vicious pseudo assembly

15
00:00:34,239 --> 00:00:37,640
instruction and we don't want to see

16
00:00:36,120 --> 00:00:41,079
that kind of thing because it's actually

17
00:00:37,640 --> 00:00:43,840
a salt instruction with a hard-coded x0

18
00:00:41,079 --> 00:00:45,840
as its source one so let's go ahead and

19
00:00:43,840 --> 00:00:48,480
get rid of that vicious pseudo assembly

20
00:00:45,840 --> 00:00:52,320
instruction and see the salt that lays

21
00:00:48,480 --> 00:00:54,960
beneath so new assembly salt set if less

22
00:00:52,320 --> 00:00:57,440
than and this is going to be a signed

23
00:00:54,960 --> 00:00:59,480
comparison unlike the salt U which was

24
00:00:57,440 --> 00:01:01,160
an unsigned comparison again same sort

25
00:00:59,480 --> 00:01:03,719
of principle as other assembly

26
00:01:01,160 --> 00:01:06,119
instructions tacking on U's for unsigned

27
00:01:03,719 --> 00:01:09,360
tacking on eyes for immediates tacking

28
00:01:06,119 --> 00:01:12,920
on W's for word siiz things so salt

29
00:01:09,360 --> 00:01:15,200
source one source two if source one is

30
00:01:12,920 --> 00:01:17,640
less than source two and that's treated

31
00:01:15,200 --> 00:01:19,479
as a signed comparison so they could be

32
00:01:17,640 --> 00:01:22,240
interpreted as negative numbers if

33
00:01:19,479 --> 00:01:24,560
appropriate so if the source one is less

34
00:01:22,240 --> 00:01:27,600
than source two set the destination

35
00:01:24,560 --> 00:01:31,000
register to one else set the destination

36
00:01:27,600 --> 00:01:34,960
register to zero so if the set greater

37
00:01:31,000 --> 00:01:37,040
than zero is a salt with a hardcoded x0

38
00:01:34,960 --> 00:01:40,720
for the source register then you'd see

39
00:01:37,040 --> 00:01:43,399
that it would be doing a zero if zero is

40
00:01:40,720 --> 00:01:45,600
less than your value and so that would

41
00:01:43,399 --> 00:01:49,159
only be set when you have positive

42
00:01:45,600 --> 00:01:50,640
values for rs2 right so set greater than

43
00:01:49,159 --> 00:01:52,680
these would have to be positive values

44
00:01:50,640 --> 00:01:54,960
greater than zero because that would not

45
00:01:52,680 --> 00:01:56,560
hold for a negative value if rs2 was

46
00:01:54,960 --> 00:01:59,680
treated as signed and it was like a

47
00:01:56,560 --> 00:02:03,680
negative 1 well netive 1 is not greater

48
00:01:59,680 --> 00:02:05,360
than than zero okay well once again stop

49
00:02:03,680 --> 00:02:07,680
step through the assembly and make sure

50
00:02:05,360 --> 00:02:10,160
you understand how this optimized code

51
00:02:07,680 --> 00:02:13,480
is using the set greater than zero or

52
00:02:10,160 --> 00:02:17,760
the salt in order to optimize down this

53
00:02:13,480 --> 00:02:17,760
comparison of a greater than zero

