1
00:00:00,080 --> 00:00:05,400
so there wasn't a whole lot to see in

2
00:00:01,959 --> 00:00:07,600
this hello world o0 but if you stepped

3
00:00:05,400 --> 00:00:09,840
through this location which was the

4
00:00:07,600 --> 00:00:12,240
movement of the frame pointer it should

5
00:00:09,840 --> 00:00:14,320
have looked like this you'd see a saved

6
00:00:12,240 --> 00:00:16,320
return address because main calls a

7
00:00:14,320 --> 00:00:18,279
different function main calls into put

8
00:00:16,320 --> 00:00:20,279
us so it needs to save the return

9
00:00:18,279 --> 00:00:21,600
address so that it can restore it later

10
00:00:20,279 --> 00:00:23,720
on when it wants to get back to the

11
00:00:21,600 --> 00:00:25,960
function that call domain additionally

12
00:00:23,720 --> 00:00:28,439
as pre usual we saw the saving of the

13
00:00:25,960 --> 00:00:30,160
frame pointer as it existed before we

14
00:00:28,439 --> 00:00:32,920
got into main so it can again be

15
00:00:30,160 --> 00:00:34,680
restored on your exit from main so this

16
00:00:32,920 --> 00:00:37,079
is the main frame and it has both the

17
00:00:34,680 --> 00:00:39,239
return address and the frame pointer so

18
00:00:37,079 --> 00:00:42,440
the first and only argument to the print

19
00:00:39,239 --> 00:00:45,360
F or put s was placed into the a or

20
00:00:42,440 --> 00:00:47,399
argument zero register and additionally

21
00:00:45,360 --> 00:00:49,719
there is one miscellaneous thing here

22
00:00:47,399 --> 00:00:51,960
there's a noop instruction for no

23
00:00:49,719 --> 00:00:56,079
apparent reason so we'll talk about why

24
00:00:51,960 --> 00:00:56,079
that exists in a couple of videos

