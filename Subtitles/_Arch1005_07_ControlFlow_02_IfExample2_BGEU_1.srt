1
00:00:00,359 --> 00:00:05,319
if example two is exactly the same as if

2
00:00:03,000 --> 00:00:08,120
example one except we've changed from

3
00:00:05,319 --> 00:00:10,200
signed longs to unsigned Longs both for

4
00:00:08,120 --> 00:00:13,639
the local variables and for the return

5
00:00:10,200 --> 00:00:15,240
value we are returning a cost for our

6
00:00:13,639 --> 00:00:17,359
return value that is dead code here it's

7
00:00:15,240 --> 00:00:19,560
either going to return one two or three

8
00:00:17,359 --> 00:00:22,439
so compile it see the new assembly

9
00:00:19,560 --> 00:00:24,720
instruction BGE and let's figure out

10
00:00:22,439 --> 00:00:27,480
what that is that is a branch if greater

11
00:00:24,720 --> 00:00:29,759
or equal unsigned so this is now going

12
00:00:27,480 --> 00:00:31,400
to be an unsigned comparison rather than

13
00:00:29,759 --> 00:00:35,640
the previous greater than equal which

14
00:00:31,400 --> 00:00:38,160
was signed so does an if rs1 greater

15
00:00:35,640 --> 00:00:40,800
than or equal rs2 and treat the register

16
00:00:38,160 --> 00:00:42,840
values as unsigned numbers so no more

17
00:00:40,800 --> 00:00:44,480
carrying if one of them is negative if

18
00:00:42,840 --> 00:00:47,320
that is true then the same thing as

19
00:00:44,480 --> 00:00:50,800
before just PC plus offset encoded by

20
00:00:47,320 --> 00:00:52,399
immediate 13 that's the new pc if not

21
00:00:50,800 --> 00:00:55,280
just follow through the next assembly

22
00:00:52,399 --> 00:00:57,199
instruction and again it's just going to

23
00:00:55,280 --> 00:00:58,719
show you an absolute address it's not

24
00:00:57,199 --> 00:01:00,920
going to show you the offset in the

25
00:00:58,719 --> 00:01:02,840
disassembler okay so not exactly a

26
00:01:00,920 --> 00:01:04,960
mental strain there so this one should

27
00:01:02,840 --> 00:01:08,640
be quick and easy to step through the

28
00:01:04,960 --> 00:01:08,640
assembly and check your understanding

