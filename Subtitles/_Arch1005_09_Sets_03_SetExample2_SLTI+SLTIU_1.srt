1
00:00:00,359 --> 00:00:04,880
okay here in set example two it's the

2
00:00:02,440 --> 00:00:07,240
same basic idea but now instead of doing

3
00:00:04,880 --> 00:00:10,599
a a greater than zero we're going to do

4
00:00:07,240 --> 00:00:12,719
an a less than seven so we're including

5
00:00:10,599 --> 00:00:15,519
some sort of hard-coded immediate that

6
00:00:12,719 --> 00:00:17,960
is not equal to zero and we're going to

7
00:00:15,519 --> 00:00:21,240
optimize this one for size to give us

8
00:00:17,960 --> 00:00:25,359
nice small thing and here we go there's

9
00:00:21,240 --> 00:00:27,519
our salty so what is that doing so set

10
00:00:25,359 --> 00:00:29,480
if less than immediate is just going to

11
00:00:27,519 --> 00:00:32,520
use an immediate for the comparison

12
00:00:29,480 --> 00:00:35,239
instead of a register so take your

13
00:00:32,520 --> 00:00:38,280
source do a less than check against the

14
00:00:35,239 --> 00:00:41,320
sign extended immediate 12 and treat it

15
00:00:38,280 --> 00:00:43,600
as a signed comparison it's not salt I

16
00:00:41,320 --> 00:00:46,360
you it's salt I and so it's defaulting

17
00:00:43,600 --> 00:00:49,039
to signed comparison if your source

18
00:00:46,360 --> 00:00:51,760
register is less than the immediate then

19
00:00:49,039 --> 00:00:54,559
set the destination to one else set it

20
00:00:51,760 --> 00:00:57,320
to zero and very quickly we'll just go

21
00:00:54,559 --> 00:00:59,359
ahead and jump into set example three

22
00:00:57,320 --> 00:01:03,120
exact same thing except we're using an

23
00:00:59,359 --> 00:01:04,400
unsigned in and what do you know a salty

24
00:01:03,120 --> 00:01:07,840
U is

25
00:01:04,400 --> 00:01:10,240
generated set less than immediate

26
00:01:07,840 --> 00:01:12,280
unsigned so set less than immediate

27
00:01:10,240 --> 00:01:14,720
unsigned exact same thing as we just saw

28
00:01:12,280 --> 00:01:17,119
with the salty but it's going to treat

29
00:01:14,720 --> 00:01:20,240
this as an unsigned comparison when it's

30
00:01:17,119 --> 00:01:22,640
doing the source less than immediate so

31
00:01:20,240 --> 00:01:24,880
in the grand tradition of RISC-V we've

32
00:01:22,640 --> 00:01:28,640
got permutations of Plenty and we just

33
00:01:24,880 --> 00:01:31,360
picked up salty and salty you salty me

34
00:01:28,640 --> 00:01:33,920
no salty you salty and salty U which are

35
00:01:31,360 --> 00:01:36,200
just like the salt and salt U they're

36
00:01:33,920 --> 00:01:38,759
basically just permutations that use an

37
00:01:36,200 --> 00:01:40,280
immediate instead of a register so read

38
00:01:38,759 --> 00:01:42,960
the code make sure you understand what

39
00:01:40,280 --> 00:01:42,960
it's doing

