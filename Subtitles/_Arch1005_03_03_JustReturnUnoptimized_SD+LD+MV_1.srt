1
00:00:00,280 --> 00:00:04,080
okay as a reminder in our previous

2
00:00:02,000 --> 00:00:06,839
example we had this code that we

3
00:00:04,080 --> 00:00:09,200
compiled with the 01 optimization and it

4
00:00:06,839 --> 00:00:11,440
gave us this assembly language the LI

5
00:00:09,200 --> 00:00:14,879
which is a lie and the r which isn't

6
00:00:11,440 --> 00:00:16,520
real so now we are going to look at an

7
00:00:14,879 --> 00:00:19,520
unoptimized version we're going to

8
00:00:16,520 --> 00:00:22,199
compile with o0 optimization level and

9
00:00:19,520 --> 00:00:24,439
that gives us this code now this green

10
00:00:22,199 --> 00:00:27,320
stuff is exactly that lion r that you

11
00:00:24,439 --> 00:00:30,199
saw before and the blue stuff is new to

12
00:00:27,320 --> 00:00:32,480
you green you've seen blue is new to you

13
00:00:30,199 --> 00:00:36,079
so additionally this brings us three new

14
00:00:32,480 --> 00:00:38,239
assembly instructions SD MV and LD now a

15
00:00:36,079 --> 00:00:40,320
quick word about words people have a

16
00:00:38,239 --> 00:00:42,840
hard time agreeing on what the word word

17
00:00:40,320 --> 00:00:46,000
means this is because it has changed

18
00:00:42,840 --> 00:00:47,760
over time originally in 16bit systems it

19
00:00:46,000 --> 00:00:50,320
meant one thing and then as we moved to

20
00:00:47,760 --> 00:00:52,160
32 and 64 it meant different things what

21
00:00:50,320 --> 00:00:56,559
we need to know for now is that the RISC

22
00:00:52,160 --> 00:00:58,480
5 ISA Define a word as 32bits regardless

23
00:00:56,559 --> 00:01:01,640
of whether or not you're using a 32-bit

24
00:00:58,480 --> 00:01:04,559
or 64-bit ISA this is okay because this

25
00:01:01,640 --> 00:01:06,840
matches gdb's terminology which uses w

26
00:01:04,559 --> 00:01:09,360
as the format specifier for a word

27
00:01:06,840 --> 00:01:12,520
furthermore RISC-V ises Define a half

28
00:01:09,360 --> 00:01:15,240
word as 16 bits again this matches gdb's

29
00:01:12,520 --> 00:01:18,720
terminology where an H for half word is

30
00:01:15,240 --> 00:01:22,720
a 16bit format specifier but here is our

31
00:01:18,720 --> 00:01:25,680
digression RISC-V call a double word

32
00:01:22,720 --> 00:01:28,000
the 64-bit value so double word for 64

33
00:01:25,680 --> 00:01:31,040
bits because a word is 32 bits here

34
00:01:28,000 --> 00:01:33,920
gdb's notation change and they call it a

35
00:01:31,040 --> 00:01:35,399
giant word and they use the G specifier

36
00:01:33,920 --> 00:01:37,360
and furthermore if you've ever done

37
00:01:35,399 --> 00:01:39,159
something like Windows programming you

38
00:01:37,360 --> 00:01:42,240
may be familiar with their usage of the

39
00:01:39,159 --> 00:01:45,040
term double word or dword to mean 32-bit

40
00:01:42,240 --> 00:01:47,799
values that's because back in the 8086

41
00:01:45,040 --> 00:01:50,280
days which were 16bit processors a word

42
00:01:47,799 --> 00:01:52,759
was 16 bits so therefore a double word

43
00:01:50,280 --> 00:01:55,280
would be 32bits but we're in Risk 5

44
00:01:52,759 --> 00:01:58,759
world so we just need to know word is 32

45
00:01:55,280 --> 00:02:01,439
half word is 16 double word is 64 so

46
00:01:58,759 --> 00:02:03,840
here we have the s SD or store double

47
00:02:01,439 --> 00:02:06,680
word from register to memory assembly

48
00:02:03,840 --> 00:02:10,119
instruction so we' got SD source 2

49
00:02:06,680 --> 00:02:12,160
source one and a immediate 12bit value

50
00:02:10,119 --> 00:02:13,440
so how this is interpreted and this is

51
00:02:12,160 --> 00:02:15,360
what it'll look like when you're looking

52
00:02:13,440 --> 00:02:18,160
at the assembly is that it's going to

53
00:02:15,360 --> 00:02:20,280
take source 2 and that's the value to

54
00:02:18,160 --> 00:02:22,040
actually be stored into memory so this

55
00:02:20,280 --> 00:02:24,160
is actually sort of reading like the

56
00:02:22,040 --> 00:02:26,120
source 2 is going out to memory it's

57
00:02:24,160 --> 00:02:27,680
moving from left to right and the memory

58
00:02:26,120 --> 00:02:30,239
location where it's going is you take

59
00:02:27,680 --> 00:02:32,319
the rs1 the source one you take whatever

60
00:02:30,239 --> 00:02:34,640
value is in that register add the sign

61
00:02:32,319 --> 00:02:36,840
extended immediate 12 to it and then

62
00:02:34,640 --> 00:02:38,879
treat all of that as an address and go

63
00:02:36,840 --> 00:02:42,200
to memory at that address and store the

64
00:02:38,879 --> 00:02:45,080
dword from rs2 which is 64-bit Value

65
00:02:42,200 --> 00:02:46,760
store that 64 bits into memory at

66
00:02:45,080 --> 00:02:49,120
whatever address you calculate so in

67
00:02:46,760 --> 00:02:52,239
General Store assembly instructions in

68
00:02:49,120 --> 00:02:54,959
Risk 5 are not like most instructions

69
00:02:52,239 --> 00:02:56,640
they're read by humans from left to

70
00:02:54,959 --> 00:02:58,319
right so whereas most things you read

71
00:02:56,640 --> 00:02:59,879
from right to left like you take the

72
00:02:58,319 --> 00:03:02,640
stuff on the right and you store it over

73
00:02:59,879 --> 00:03:05,159
into a register on the left stores

74
00:03:02,640 --> 00:03:08,239
specifically are read from left to right

75
00:03:05,159 --> 00:03:10,840
so take whatever is on the left and

76
00:03:08,239 --> 00:03:12,920
store it out to memory at some address

77
00:03:10,840 --> 00:03:15,519
specified on the right rs2 goes into

78
00:03:12,920 --> 00:03:17,440
dword memory at rs1 plus sign extended

79
00:03:15,519 --> 00:03:20,440
immediate 12 furthermore whenever you're

80
00:03:17,440 --> 00:03:22,480
looking at assembly in GDB if you see

81
00:03:20,440 --> 00:03:24,519
these parentheses that's sort of your

82
00:03:22,480 --> 00:03:26,480
hint that there's some sort of memory

83
00:03:24,519 --> 00:03:27,959
address or memory dereferencing

84
00:03:26,480 --> 00:03:30,519
occurring so in the same way that you

85
00:03:27,959 --> 00:03:32,280
know if you dreference a pointer in C by

86
00:03:30,519 --> 00:03:33,799
putting like a star in front of it the

87
00:03:32,280 --> 00:03:36,239
syntax that they use for memory

88
00:03:33,799 --> 00:03:38,040
references are these parentheses that's

89
00:03:36,239 --> 00:03:40,439
usually parentheses for some base

90
00:03:38,040 --> 00:03:42,280
address and some sort of immediate

91
00:03:40,439 --> 00:03:44,599
outside of the parentheses that's added

92
00:03:42,280 --> 00:03:47,120
on to calculate the ultimate overall

93
00:03:44,599 --> 00:03:49,080
address to be written to or read from so

94
00:03:47,120 --> 00:03:52,360
if that store so you're storing out to

95
00:03:49,080 --> 00:03:56,760
memory we have the CounterPoint in load

96
00:03:52,360 --> 00:03:59,480
LD load double word from memory into

97
00:03:56,760 --> 00:04:01,760
register so here we have a destination

98
00:03:59,480 --> 00:04:03,400
on on this side we have the source one

99
00:04:01,760 --> 00:04:06,120
and so basically you're going to take

100
00:04:03,400 --> 00:04:09,079
this source one add in the sign extended

101
00:04:06,120 --> 00:04:11,159
immediate 12-bit value calculate a

102
00:04:09,079 --> 00:04:14,120
memory address dreference that memory

103
00:04:11,159 --> 00:04:16,600
address grab the dword from that memory

104
00:04:14,120 --> 00:04:18,959
address and store it into the Rd

105
00:04:16,600 --> 00:04:21,959
destination register so like most

106
00:04:18,959 --> 00:04:24,680
instructions and unlike stores loads are

107
00:04:21,959 --> 00:04:26,759
read from right to left so the stuff

108
00:04:24,680 --> 00:04:28,199
over here on the right goes into this

109
00:04:26,759 --> 00:04:31,080
stuff on the left the destination

110
00:04:28,199 --> 00:04:34,199
register so again RS 1 plus sign

111
00:04:31,080 --> 00:04:36,560
extended immediate 12 treat it as dword

112
00:04:34,199 --> 00:04:39,080
and pull the dword out of memory and

113
00:04:36,560 --> 00:04:41,440
stick it into the Rd register and again

114
00:04:39,080 --> 00:04:43,240
just the presence of parentheses should

115
00:04:41,440 --> 00:04:45,600
be the hint that there's some sort of

116
00:04:43,240 --> 00:04:47,520
memory reference memory D reference

117
00:04:45,600 --> 00:04:49,639
being used in this assembly instruction

118
00:04:47,520 --> 00:04:51,919
so then we go and look at our assembly

119
00:04:49,639 --> 00:04:54,400
instructions but we are not going to

120
00:04:51,919 --> 00:04:56,919
find LD and SD here we look at all these

121
00:04:54,400 --> 00:04:58,400
other L's and other s's but we don't

122
00:04:56,919 --> 00:05:00,160
find it and if you were paying attention

123
00:04:58,400 --> 00:05:02,000
on the previous slide you'll know why

124
00:05:00,160 --> 00:05:04,880
that's because the LD and SD are

125
00:05:02,000 --> 00:05:06,840
actually part of the RV64I base

126
00:05:04,880 --> 00:05:09,840
instruction set they are some of these

127
00:05:06,840 --> 00:05:13,479
additional instructions above and beyond

128
00:05:09,840 --> 00:05:15,720
the RV32I so we've got LD and SD right

129
00:05:13,479 --> 00:05:17,199
there happy and healthy but we don't

130
00:05:15,720 --> 00:05:19,479
care about all of this instruction

131
00:05:17,199 --> 00:05:21,600
encoding so for now we're just going to

132
00:05:19,479 --> 00:05:23,960
take those and add it to our list we've

133
00:05:21,600 --> 00:05:27,600
got our 40 base instructions our 15

134
00:05:23,960 --> 00:05:29,759
extra instructions specific to RV64I

135
00:05:27,600 --> 00:05:31,319
and so that we picked up a couple of

136
00:05:29,759 --> 00:05:33,160
those and we'll denote that here in a

137
00:05:31,319 --> 00:05:36,120
second so one other assembly instruction

138
00:05:33,160 --> 00:05:38,199
we saw here was move MV which moves a

139
00:05:36,120 --> 00:05:40,080
register into a different register so it

140
00:05:38,199 --> 00:05:41,880
just takes your source from the right

141
00:05:40,080 --> 00:05:44,120
and moves it into the destination on the

142
00:05:41,880 --> 00:05:48,000
left and so it's read like most other

143
00:05:44,120 --> 00:05:48,000
assembly instructions right to left

