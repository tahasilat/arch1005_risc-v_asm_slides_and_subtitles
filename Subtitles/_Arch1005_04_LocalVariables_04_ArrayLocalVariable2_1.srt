1
00:00:00,199 --> 00:00:04,799
now as a reminder what we just covered

2
00:00:02,200 --> 00:00:08,040
was array local variable. C and that had

3
00:00:04,799 --> 00:00:10,759
a six long long array followed by a

4
00:00:08,040 --> 00:00:12,759
single long long now let's do a small

5
00:00:10,759 --> 00:00:14,960
permutation on that array local variable

6
00:00:12,759 --> 00:00:16,720
two we're going to break up that a that

7
00:00:14,960 --> 00:00:18,480
was six long Longs before we're going to

8
00:00:16,720 --> 00:00:20,920
make it three and we're going to give it

9
00:00:18,480 --> 00:00:22,880
explicit initialization with an implicit

10
00:00:20,920 --> 00:00:25,160
zero and then we're going to have

11
00:00:22,880 --> 00:00:26,880
another three that will call C down here

12
00:00:25,160 --> 00:00:29,039
with no initialization and we're going

13
00:00:26,880 --> 00:00:31,519
to take the same initialization here B

14
00:00:29,039 --> 00:00:33,120
to that magic value a to that magic

15
00:00:31,519 --> 00:00:35,399
value which one added together will give

16
00:00:33,120 --> 00:00:38,280
us B bled blood but we're going to store

17
00:00:35,399 --> 00:00:40,640
it this time into C of two and return C

18
00:00:38,280 --> 00:00:43,039
of2 instead of returning a of4 like we

19
00:00:40,640 --> 00:00:44,960
did in the last one so the question

20
00:00:43,039 --> 00:00:47,360
we're posing here and the inference we

21
00:00:44,960 --> 00:00:50,320
want to gain from this is that we saw

22
00:00:47,360 --> 00:00:52,199
with A and B that they were array and a

23
00:00:50,320 --> 00:00:54,000
scaler and they were allocated in

24
00:00:52,199 --> 00:00:57,079
basically the same way that we've seen

25
00:00:54,000 --> 00:00:58,879
thus far for unoptimized code the exact

26
00:00:57,079 --> 00:01:02,079
sort of ordering just reversed because

27
00:00:58,879 --> 00:01:05,799
of our stack diagram but now we've got a

28
00:01:02,079 --> 00:01:07,720
an array B A scaler and C an array we

29
00:01:05,799 --> 00:01:10,000
want to know are they going to still be

30
00:01:07,720 --> 00:01:11,880
an exactly that order will BB in the

31
00:01:10,000 --> 00:01:15,520
middle will B be at the beginning will

32
00:01:11,880 --> 00:01:17,840
BB at the end we don't know until we

33
00:01:15,520 --> 00:01:20,920
compile it and step through the code no

34
00:01:17,840 --> 00:01:23,320
new assembly here no surprise so go

35
00:01:20,920 --> 00:01:26,360
ahead and start with your initialized

36
00:01:23,320 --> 00:01:28,520
values and create your own stack diagram

37
00:01:26,360 --> 00:01:31,560
based on stepping through this code in

38
00:01:28,520 --> 00:01:31,560
the debugger

