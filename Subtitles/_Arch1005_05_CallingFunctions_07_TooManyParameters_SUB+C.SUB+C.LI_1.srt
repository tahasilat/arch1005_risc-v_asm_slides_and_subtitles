1
00:00:00,160 --> 00:00:03,719
okay so at this point we get the gist of

2
00:00:01,959 --> 00:00:06,759
argument passing we know that they go

3
00:00:03,719 --> 00:00:09,360
into a0 a1 a2 but let's go ahead and

4
00:00:06,759 --> 00:00:12,040
completely overdo it past 10 arguments

5
00:00:09,360 --> 00:00:14,799
because we know that only eight argument

6
00:00:12,040 --> 00:00:16,720
registers exist and after that they have

7
00:00:14,799 --> 00:00:18,039
to go on the stack so let's see exactly

8
00:00:16,720 --> 00:00:20,080
where they are put on the stack how

9
00:00:18,039 --> 00:00:22,960
they're put on the stack and so forth so

10
00:00:20,080 --> 00:00:26,560
this function main calls func with 10

11
00:00:22,960 --> 00:00:28,679
arguments and then func has a uint64

12
00:00:26,560 --> 00:00:30,640
defined as unsigned long long takes the

13
00:00:28,679 --> 00:00:32,200
10 arguments and just does some math on

14
00:00:30,640 --> 00:00:34,600
them addition subtraction addition

15
00:00:32,200 --> 00:00:37,480
subtraction calculates a result and

16
00:00:34,600 --> 00:00:39,920
returns that result so that code

17
00:00:37,480 --> 00:00:41,760
generates this assembly and we're going

18
00:00:39,920 --> 00:00:44,399
to pick up a new assembly instruction

19
00:00:41,760 --> 00:00:46,480
sub it's a little bit too big to fit on

20
00:00:44,399 --> 00:00:48,840
the screen because we have so many add

21
00:00:46,480 --> 00:00:50,719
subtracts add subtracts so what does the

22
00:00:48,840 --> 00:00:53,079
sub instruction do well what you would

23
00:00:50,719 --> 00:00:55,600
expect it to do subtracts a register

24
00:00:53,079 --> 00:00:58,640
from a register so it's written in this

25
00:00:55,600 --> 00:01:00,960
form source one source two so register

26
00:00:58,640 --> 00:01:03,760
source one minus Reg register source 2

27
00:01:00,960 --> 00:01:05,840
the result is placed back into register

28
00:01:03,760 --> 00:01:08,000
destination so if you saw an assembly

29
00:01:05,840 --> 00:01:12,360
instruction like this you would be doing

30
00:01:08,000 --> 00:01:14,720
x2 - x3 storing it back into x1 so sub

31
00:01:12,360 --> 00:01:18,200
like add intentionally does not have a

32
00:01:14,720 --> 00:01:20,200
two register form in RV32I you have to

33
00:01:18,200 --> 00:01:22,680
specify all three registers so there's

34
00:01:20,200 --> 00:01:25,040
no subtract that implicitly uses the

35
00:01:22,680 --> 00:01:26,840
destination as a source it's always

36
00:01:25,040 --> 00:01:30,079
source one source

37
00:01:26,840 --> 00:01:32,680
two okay well let's go ahead and take a

38
00:01:30,079 --> 00:01:34,960
look at the code draw a stack diagram

39
00:01:32,680 --> 00:01:36,960
and see what goes where with all of

40
00:01:34,960 --> 00:01:39,920
these registers being thrown all around

41
00:01:36,960 --> 00:01:41,560
for all these arguments stop look

42
00:01:39,920 --> 00:01:43,399
through the assembly draw a stack

43
00:01:41,560 --> 00:01:46,799
diagram you're used to this at this

44
00:01:43,399 --> 00:01:46,799
point right

