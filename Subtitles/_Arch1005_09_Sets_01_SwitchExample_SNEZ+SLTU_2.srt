1
00:00:00,080 --> 00:00:03,280
so your understanding of the salt U

2
00:00:01,839 --> 00:00:05,319
assembly instruction should have been

3
00:00:03,280 --> 00:00:07,160
pretty simple but understanding how it

4
00:00:05,319 --> 00:00:09,320
was used and why it was used in the

5
00:00:07,160 --> 00:00:12,080
context of this switch that is what was

6
00:00:09,320 --> 00:00:14,480
a little more interesting so as we see

7
00:00:12,080 --> 00:00:16,520
in this code we've got a2i going into a

8
00:00:14,480 --> 00:00:18,279
so it is a signed value and then it's

9
00:00:16,520 --> 00:00:20,960
switching and it's saying is it zero is

10
00:00:18,279 --> 00:00:24,240
it one or is it anything else so in the

11
00:00:20,960 --> 00:00:27,240
assembly we had the a z after the call

12
00:00:24,240 --> 00:00:29,800
to string to long a z is going to be the

13
00:00:27,240 --> 00:00:32,880
return value and then we have an add w

14
00:00:29,800 --> 00:00:35,719
at adding zero and putting it into a5 so

15
00:00:32,880 --> 00:00:38,480
w that's the word sized addition and

16
00:00:35,719 --> 00:00:40,399
subsequent sign extension then it puts

17
00:00:38,480 --> 00:00:43,600
the then it loads an immediate it puts

18
00:00:40,399 --> 00:00:45,960
one into the a Zer register so the a is

19
00:00:43,600 --> 00:00:48,760
here in a5 now and a hard-coded one is

20
00:00:45,960 --> 00:00:52,039
placed into a z and then it does a

21
00:00:48,760 --> 00:00:54,760
branch if equal to zero on a5 so this

22
00:00:52,039 --> 00:00:56,559
input value it's saying if that's a5

23
00:00:54,760 --> 00:00:58,680
then we need to go somewhere or sorry if

24
00:00:56,559 --> 00:01:01,239
that's zero then we need to go somewhere

25
00:00:58,680 --> 00:01:03,160
and return one so I put one into the

26
00:01:01,239 --> 00:01:07,040
return and then it says if that input

27
00:01:03,160 --> 00:01:09,040
was one branch to main + 30 go down here

28
00:01:07,040 --> 00:01:10,560
main plus 30 and that's just the

29
00:01:09,040 --> 00:01:13,479
function epilog it's just going to be

30
00:01:10,560 --> 00:01:16,159
exiting out with a return value of one

31
00:01:13,479 --> 00:01:18,240
however if it was not zero then it's

32
00:01:16,159 --> 00:01:20,880
going to fall through and it's going to

33
00:01:18,240 --> 00:01:23,720
add a negative - one to the input value

34
00:01:20,880 --> 00:01:26,000
so if the input value was one then

35
00:01:23,720 --> 00:01:28,799
adding a negative one will'll have the

36
00:01:26,000 --> 00:01:30,400
register be zero and then if it does a

37
00:01:28,799 --> 00:01:34,759
set less than

38
00:01:30,400 --> 00:01:37,360
unsigned comparison between zero and a5

39
00:01:34,759 --> 00:01:40,240
if the input was one at this point a5

40
00:01:37,360 --> 00:01:42,960
will be Zer and it's going to do is0

41
00:01:40,240 --> 00:01:45,680
less than zero and so that would be

42
00:01:42,960 --> 00:01:47,880
setting it if the or it would be setting

43
00:01:45,680 --> 00:01:50,920
it to zero it would be setting a z to

44
00:01:47,880 --> 00:01:52,880
zero if the input was one otherwise it

45
00:01:50,920 --> 00:01:54,799
would be setting it to one so then it

46
00:01:52,880 --> 00:01:57,320
goes down here so it's either going to

47
00:01:54,799 --> 00:01:59,360
have a zero or one in the a z register

48
00:01:57,320 --> 00:02:01,399
at this point and therefore when it

49
00:01:59,360 --> 00:02:03,560
falls through to the function EP log

50
00:02:01,399 --> 00:02:05,640
it's either going to be adding 2 to zero

51
00:02:03,560 --> 00:02:07,799
or 2 to one and therefore it's either

52
00:02:05,640 --> 00:02:10,879
going to be returning two or returning

53
00:02:07,799 --> 00:02:13,239
three so if this was one which is only

54
00:02:10,879 --> 00:02:15,360
going to be if the input value was

55
00:02:13,239 --> 00:02:17,440
something greater than one so anything

56
00:02:15,360 --> 00:02:19,959
else besides one and it would have got a

57
00:02:17,440 --> 00:02:21,720
one here and then it will return three

58
00:02:19,959 --> 00:02:23,920
so yeah interesting little tricks like

59
00:02:21,720 --> 00:02:27,160
that when you use the optimization level

60
00:02:23,920 --> 00:02:29,040
to and you get nice compact assembly all

61
00:02:27,160 --> 00:02:31,000
right now I said that I want you to say

62
00:02:29,040 --> 00:02:33,840
salt when you are pronouncing these

63
00:02:31,000 --> 00:02:38,280
assembly instructions and that's because

64
00:02:33,840 --> 00:02:41,879
the salt U and the salt and the salt I U

65
00:02:38,280 --> 00:02:43,519
or salty U and salty these are you know

66
00:02:41,879 --> 00:02:44,920
sometimes easy when you're just skimming

67
00:02:43,519 --> 00:02:47,640
through assembly you've got all these

68
00:02:44,920 --> 00:02:50,440
other things like shift right so shift

69
00:02:47,640 --> 00:02:53,440
logical left looks very similar to a

70
00:02:50,440 --> 00:02:56,440
salt right but this is not a shift left

71
00:02:53,440 --> 00:02:58,760
something with the t is just a salt so

72
00:02:56,440 --> 00:03:01,720
if you can say salt then you know that

73
00:02:58,760 --> 00:03:03,799
this is going to be one of these set if

74
00:03:01,720 --> 00:03:06,239
instructions specifically set if less

75
00:03:03,799 --> 00:03:10,040
than so we've got salt you and we've got

76
00:03:06,239 --> 00:03:12,760
snz and you're going to use the salt uh

77
00:03:10,040 --> 00:03:14,200
pronunciation in order to keep track of

78
00:03:12,760 --> 00:03:16,400
these variants which are going to be up

79
00:03:14,200 --> 00:03:18,640
here about setting less than and help

80
00:03:16,400 --> 00:03:20,879
you differentiate from all of these

81
00:03:18,640 --> 00:03:22,920
various shifts when you're just skimming

82
00:03:20,879 --> 00:03:24,959
through code so we're going to say salt

83
00:03:22,920 --> 00:03:26,080
through the rest of the new instruction

84
00:03:24,959 --> 00:03:29,080
variants that we're going to learn in

85
00:03:26,080 --> 00:03:29,080
the next sections

