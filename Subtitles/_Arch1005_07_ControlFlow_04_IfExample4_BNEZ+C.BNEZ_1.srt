1
00:00:00,080 --> 00:00:05,799
in if example 4 we have again pulling in

2
00:00:03,560 --> 00:00:08,320
some value from the command line running

3
00:00:05,799 --> 00:00:11,040
it through a2l putting it into the long

4
00:00:08,320 --> 00:00:14,320
a and then checking if a not equal to

5
00:00:11,040 --> 00:00:16,320
zero return one else return deceit now

6
00:00:14,320 --> 00:00:17,960
you might say I think I've already seen

7
00:00:16,320 --> 00:00:21,320
this comparison before it's just going

8
00:00:17,960 --> 00:00:23,000
to create a branch if not equal wrong

9
00:00:21,320 --> 00:00:25,720
it's going to create a branch if not

10
00:00:23,000 --> 00:00:27,880
equal to zero but I can tell you already

11
00:00:25,720 --> 00:00:31,199
that that's a pseudo instruction

12
00:00:27,880 --> 00:00:33,399
horrible hideous ah kill it with fire ah

13
00:00:31,199 --> 00:00:35,840
the fire isn't working we're going to

14
00:00:33,399 --> 00:00:38,079
have to just get used to the fact that

15
00:00:35,840 --> 00:00:40,160
branch if not equal to zero is the same

16
00:00:38,079 --> 00:00:42,320
thing as a branch if not equal where you

17
00:00:40,160 --> 00:00:44,440
put a zero in there so branch if not

18
00:00:42,320 --> 00:00:46,559
equal to zero not going to do a build

19
00:00:44,440 --> 00:00:49,559
slide for this it's just if the source

20
00:00:46,559 --> 00:00:51,600
not equal to zero then branch and the

21
00:00:49,559 --> 00:00:53,680
other thing besides the source is going

22
00:00:51,600 --> 00:00:56,399
to be the hard-coded zero register so

23
00:00:53,680 --> 00:00:58,680
branch if not equal register source not

24
00:00:56,399 --> 00:01:00,320
equal to zero branch to the offset all

25
00:00:58,680 --> 00:01:02,640
right well you can't kill a pseudo

26
00:01:00,320 --> 00:01:05,280
instruction with fire but maybe some

27
00:01:02,640 --> 00:01:08,560
chaos magic will work let's go ahead and

28
00:01:05,280 --> 00:01:10,560
blow it away oh no it's a compressed

29
00:01:08,560 --> 00:01:13,880
branch if not equal to zero so that

30
00:01:10,560 --> 00:01:15,520
didn't really work did it well anyways

31
00:01:13,880 --> 00:01:17,439
you can go ahead and step through the

32
00:01:15,520 --> 00:01:21,479
assembly and check your understanding

33
00:01:17,439 --> 00:01:21,479
and I'll be waiting right here for you

