1
00:00:00,280 --> 00:00:04,000
well Kylo wants more and I've got more

2
00:00:02,200 --> 00:00:06,440
I've got more than he can handle so

3
00:00:04,000 --> 00:00:08,080
let's do this all right we learned about

4
00:00:06,440 --> 00:00:10,639
this we saw there was the immediate 12

5
00:00:08,080 --> 00:00:12,759
bits as these most significant 12 bits

6
00:00:10,639 --> 00:00:14,639
but then source one and destination one

7
00:00:12,759 --> 00:00:17,039
how do we encode those five bits to

8
00:00:14,639 --> 00:00:19,279
specify any given register well I

9
00:00:17,039 --> 00:00:21,039
couldn't find a easy obvious place in

10
00:00:19,279 --> 00:00:23,039
the manual that actually says that

11
00:00:21,039 --> 00:00:24,840
straight up if you find it let me know

12
00:00:23,039 --> 00:00:26,960
and I'll put a link at the below the

13
00:00:24,840 --> 00:00:28,320
video but basically it turns out by

14
00:00:26,960 --> 00:00:30,880
looking at some real instruction

15
00:00:28,320 --> 00:00:33,800
encodings that it is very Tri encoding

16
00:00:30,880 --> 00:00:35,640
of use the five bits interpret it as a

17
00:00:33,800 --> 00:00:38,079
decimal value and that's the register

18
00:00:35,640 --> 00:00:40,399
you're talking about so five bits of

19
00:00:38,079 --> 00:00:43,800
zeros would be interpreted as decimal Z

20
00:00:40,399 --> 00:00:47,039
and that means it encodes x0 five bits

21
00:00:43,800 --> 00:00:51,960
ones means it's decimal 31 and that is

22
00:00:47,039 --> 00:00:54,680
the x31 register so if this was 7 or 13

23
00:00:51,960 --> 00:00:57,160
or 15 I was going to specify a number

24
00:00:54,680 --> 00:00:58,559
that was too big whatever the number is

25
00:00:57,160 --> 00:01:00,559
that is going to be the register that's

26
00:00:58,559 --> 00:01:03,000
actually encoded great so now we know

27
00:01:00,559 --> 00:01:05,840
what those do what about this op code

28
00:01:03,000 --> 00:01:07,560
thing here seven bits at the beginning

29
00:01:05,840 --> 00:01:10,080
of the instruction the least significant

30
00:01:07,560 --> 00:01:12,759
seven bits and it seems to be called

31
00:01:10,080 --> 00:01:15,759
something called opim for all of these

32
00:01:12,759 --> 00:01:18,080
instructions so what's that well going

33
00:01:15,759 --> 00:01:20,560
back to this giant list of all sorts of

34
00:01:18,080 --> 00:01:22,920
encodings that I took as my basis I like

35
00:01:20,560 --> 00:01:24,799
broke this list up and I showed you 20

36
00:01:22,920 --> 00:01:26,280
instructions on one side and 20 on the

37
00:01:24,799 --> 00:01:28,159
other this was my basis for our

38
00:01:26,280 --> 00:01:29,520
checklist as we went through the class

39
00:01:28,159 --> 00:01:31,600
but I said we didn't want to get into

40
00:01:29,520 --> 00:01:35,119
all these instruction codings well now

41
00:01:31,600 --> 00:01:36,920
we do so let's Circle our ad I example

42
00:01:35,119 --> 00:01:39,600
and we see that the bottom seven bits

43
00:01:36,920 --> 00:01:42,360
are 0 0 1

44
00:01:39,600 --> 00:01:45,399
011 all right take that value move over

45
00:01:42,360 --> 00:01:48,920
to this next table 24.1 which says RISC

46
00:01:45,399 --> 00:01:51,880
5 base op code map and it also says inst

47
00:01:48,920 --> 00:01:53,719
one colon 0 equals 1 one so what that's

48
00:01:51,880 --> 00:01:56,640
trying to tell you is that every

49
00:01:53,719 --> 00:02:00,079
instruction will always have bits zero

50
00:01:56,640 --> 00:02:03,640
and bit one hardcoded to11 so every

51
00:02:00,079 --> 00:02:06,119
instruction for RV32I and RV64I the

52
00:02:03,640 --> 00:02:09,160
bottom seven bits the bottom two bits

53
00:02:06,119 --> 00:02:10,679
will always be one cool so now let's

54
00:02:09,160 --> 00:02:14,519
take that value we circled on the

55
00:02:10,679 --> 00:02:17,840
previous slide up M corresponded to 00 1

56
00:02:14,519 --> 00:02:21,319
0 011 so I've given you the bit indices

57
00:02:17,840 --> 00:02:24,879
beneath it so zero and one always one 2

58
00:02:21,319 --> 00:02:28,959
3 4 that's going to be 100 so this right

59
00:02:24,879 --> 00:02:31,680
here is saying inst 432 is this row

60
00:02:28,959 --> 00:02:36,120
right here so 100 brings us over to this

61
00:02:31,680 --> 00:02:38,239
column and then bits 6 and five is 0 0

62
00:02:36,120 --> 00:02:40,760
so that brings us down to this row so

63
00:02:38,239 --> 00:02:45,239
over this column down to this row and we

64
00:02:40,760 --> 00:02:53,040
have opim so the encoding for opim is 00

65
00:02:45,239 --> 00:02:55,159
1 0 0 1 1 0 0 1 0 0 1 one great so going

66
00:02:53,040 --> 00:02:59,319
back to this we know that very literally

67
00:02:55,159 --> 00:03:01,800
upim will be 00 0 1 0 011 it'll always

68
00:02:59,319 --> 00:03:03,879
be that exact same value but if it's

69
00:03:01,800 --> 00:03:05,760
that exact same value for these

70
00:03:03,879 --> 00:03:08,319
instructions and it's the same value for

71
00:03:05,760 --> 00:03:10,760
these instructions that means something

72
00:03:08,319 --> 00:03:13,720
else must be telling us whether an

73
00:03:10,760 --> 00:03:16,200
instruction is an ADDI or a salty or a

74
00:03:13,720 --> 00:03:19,080
salty U and that something else is the

75
00:03:16,200 --> 00:03:20,760
func 3 but let me just do couple quick

76
00:03:19,080 --> 00:03:23,879
more examples of op code and then we'll

77
00:03:20,760 --> 00:03:26,599
come back to func three so let's Circle

78
00:03:23,879 --> 00:03:31,319
something else we've got Jer right here

79
00:03:26,599 --> 00:03:37,519
we know that the op code is 1 01 1 one

80
00:03:31,319 --> 00:03:40,920
so 1 1 0 0 1 1 1 so down this we've got

81
00:03:37,519 --> 00:03:43,400
one1 over here and 001 over here and

82
00:03:40,920 --> 00:03:45,879
that brings us to jlla right here but

83
00:03:43,400 --> 00:03:48,000
the true value of this table in the

84
00:03:45,879 --> 00:03:50,000
documentation is to be able to see

85
00:03:48,000 --> 00:03:53,319
something saying like hey this has an OP

86
00:03:50,000 --> 00:03:55,439
code of op 32 and then being able to go

87
00:03:53,319 --> 00:03:57,200
backwards and say okay well if I know

88
00:03:55,439 --> 00:03:59,159
it's op 32 how would it actually be

89
00:03:57,200 --> 00:04:01,680
encoded well you know the bottom bits

90
00:03:59,159 --> 00:04:04,439
are going to always be 1 one and then if

91
00:04:01,680 --> 00:04:07,360
you want to know bits 43 and 2 you go up

92
00:04:04,439 --> 00:04:10,760
and you say 1 1 Z and if you want to KN

93
00:04:07,360 --> 00:04:13,360
know the bits six and five you go over

94
00:04:10,760 --> 00:04:15,840
and it's going to be 01 so from this

95
00:04:13,360 --> 00:04:17,680
table we could literally say that if you

96
00:04:15,840 --> 00:04:20,440
ever see an instruction and it says it

97
00:04:17,680 --> 00:04:23,040
has op 32 it's going to literally have

98
00:04:20,440 --> 00:04:25,759
the value binary

99
00:04:23,040 --> 00:04:27,720
011101 one so that's what this this

100
00:04:25,759 --> 00:04:31,160
table is best for is the reverse lookup

101
00:04:27,720 --> 00:04:34,039
of these mnemonics to the actual uh

102
00:04:31,160 --> 00:04:36,120
binary values so a little random bit of

103
00:04:34,039 --> 00:04:37,880
trivia that's somewhat useful somewhat

104
00:04:36,120 --> 00:04:39,759
not that's why it's trivia is that if

105
00:04:37,880 --> 00:04:41,440
the two least significant bits are

106
00:04:39,759 --> 00:04:43,840
always going to be one then let's say

107
00:04:41,440 --> 00:04:45,360
you're looking at some hex dump of some

108
00:04:43,840 --> 00:04:46,840
you know embedded system and you don't

109
00:04:45,360 --> 00:04:49,320
have any idea which architecture it's

110
00:04:46,840 --> 00:04:52,400
using is it using arm is it using x86 is

111
00:04:49,320 --> 00:04:54,600
it using myips is it using RISC-V well

112
00:04:52,400 --> 00:04:58,479
this little property of always having

113
00:04:54,600 --> 00:05:00,080
all of the RV32I and 64i instructions

114
00:04:58,479 --> 00:05:03,400
have the least significant bits of one

115
00:05:00,080 --> 00:05:06,800
one means that you could say if there is

116
00:05:03,400 --> 00:05:09,639
a uncommon occurrence of lots of Threes

117
00:05:06,800 --> 00:05:10,960
And Sevens and B's and Fs all things

118
00:05:09,639 --> 00:05:12,880
that are going to have a one one at

119
00:05:10,960 --> 00:05:15,520
their least significant bit and those

120
00:05:12,880 --> 00:05:17,720
occur on four byte boundaries then that

121
00:05:15,520 --> 00:05:19,600
might be a way to infer that perhaps you

122
00:05:17,720 --> 00:05:22,600
should try disassembling this code AS

123
00:05:19,600 --> 00:05:25,240
RISC-V now the unfortunate problem with

124
00:05:22,600 --> 00:05:26,680
that of course is the 6 uh the 16bit

125
00:05:25,240 --> 00:05:28,160
encoding which is going to give you two

126
00:05:26,680 --> 00:05:29,560
byte chunks which is going to make it so

127
00:05:28,160 --> 00:05:31,160
that you're not always having 4 by

128
00:05:29,560 --> 00:05:32,800
boundaries but I thought I would just

129
00:05:31,160 --> 00:05:35,199
throw this in there because sometimes

130
00:05:32,800 --> 00:05:36,800
you will see security researchers point

131
00:05:35,199 --> 00:05:38,680
out that like oh yeah I've looked at a

132
00:05:36,800 --> 00:05:40,800
lot of you know arm assembly and I can

133
00:05:38,680 --> 00:05:42,160
tell just based on you know the hex dump

134
00:05:40,800 --> 00:05:44,360
that there's going to be a lot of this

135
00:05:42,160 --> 00:05:47,160
value there and that is a way that you

136
00:05:44,360 --> 00:05:48,479
can actually tell in certain hex dumps

137
00:05:47,160 --> 00:05:50,319
that it is a particular architecture

138
00:05:48,479 --> 00:05:51,840
there's certain tics that come up and

139
00:05:50,319 --> 00:05:54,280
this is just the first one that jumped

140
00:05:51,840 --> 00:05:56,199
out to me as I was learning about RISC-V

141
00:05:54,280 --> 00:05:58,919
okay so let's get back to that func

142
00:05:56,199 --> 00:06:02,280
three we already learned that opim has a

143
00:05:58,919 --> 00:06:04,360
specific hardcoded 7bit value which

144
00:06:02,280 --> 00:06:07,280
means that can't be differentiating

145
00:06:04,360 --> 00:06:10,440
what's ADI versus salty versus salty U

146
00:06:07,280 --> 00:06:12,680
versus Ori versus Zori so that falls to

147
00:06:10,440 --> 00:06:14,720
the func 3 now the manual doesn't do a

148
00:06:12,680 --> 00:06:17,319
good job of like actually telling you

149
00:06:14,720 --> 00:06:20,000
what func 3 is we can find some

150
00:06:17,319 --> 00:06:21,960
references to it but we kind of as usual

151
00:06:20,000 --> 00:06:23,880
have to play some inference game first

152
00:06:21,960 --> 00:06:25,960
reference is down here when it's talking

153
00:06:23,880 --> 00:06:29,880
about adds and subtracts and ands and

154
00:06:25,960 --> 00:06:33,120
ores it says the func 7 and func 3 field

155
00:06:29,880 --> 00:06:35,520
select the type of operation okay so we

156
00:06:33,120 --> 00:06:36,919
know the op code for all of these is op

157
00:06:35,520 --> 00:06:38,880
whatever that is we could look it up in

158
00:06:36,919 --> 00:06:40,639
the table if we cared but we don't we

159
00:06:38,880 --> 00:06:43,639
know all these assembly instructions

160
00:06:40,639 --> 00:06:46,000
have the exact same bottom seven bits we

161
00:06:43,639 --> 00:06:48,240
can also see that it said func s and fun

162
00:06:46,000 --> 00:06:50,160
three well fun s it's telling us

163
00:06:48,240 --> 00:06:52,080
literally for these instructions is all

164
00:06:50,160 --> 00:06:53,919
zeros and for these instructions is all

165
00:06:52,080 --> 00:06:56,879
zeros and for these instructions is all

166
00:06:53,919 --> 00:06:58,960
zeros and then for these it's Z one and

167
00:06:56,879 --> 00:07:01,560
zeros so that means this Big Blob of

168
00:06:58,960 --> 00:07:04,080
code they have the exact same seven bits

169
00:07:01,560 --> 00:07:06,759
here they have the exact same seven bits

170
00:07:04,080 --> 00:07:08,639
here so that means that it must be func

171
00:07:06,759 --> 00:07:11,520
three which is actually doing the heavy

172
00:07:08,639 --> 00:07:13,039
lifting of figuring out which is which

173
00:07:11,520 --> 00:07:15,199
and if it's three bits we know that it

174
00:07:13,039 --> 00:07:18,319
can encode eight possible values two to

175
00:07:15,199 --> 00:07:21,319
the three and indeed we have 1 2 3 4 5 6

176
00:07:18,319 --> 00:07:23,720
7 eight so these eight values must be

177
00:07:21,319 --> 00:07:26,520
specified by the eight possible values

178
00:07:23,720 --> 00:07:28,000
of this 3 bit field but this is the only

179
00:07:26,520 --> 00:07:29,800
place that I've found thus far that

180
00:07:28,000 --> 00:07:32,759
really you know actually kind it tells

181
00:07:29,800 --> 00:07:34,759
you what fun 3 is and it says you know

182
00:07:32,759 --> 00:07:37,039
as a aside a quirk of the encoding

183
00:07:34,759 --> 00:07:40,120
format is that the 3-bit fun 3 field

184
00:07:37,039 --> 00:07:41,479
used to encode a minor op code is not

185
00:07:40,120 --> 00:07:42,919
contiguous with the major op code bits

186
00:07:41,479 --> 00:07:44,520
of the 32-bit blah blah blah we don't

187
00:07:42,919 --> 00:07:46,000
care about the rest of it what I care

188
00:07:44,520 --> 00:07:49,000
about is that here they were finally

189
00:07:46,000 --> 00:07:51,759
admitting that fun 3 is essentially a

190
00:07:49,000 --> 00:07:53,360
minor op code this is the major op code

191
00:07:51,759 --> 00:07:55,199
but then the thing that actually

192
00:07:53,360 --> 00:07:58,080
distinguishes an ad from assault from

193
00:07:55,199 --> 00:08:00,120
assault you is this minor op code the

194
00:07:58,080 --> 00:08:02,400
funct three so one should always think

195
00:08:00,120 --> 00:08:04,159
of fun three or you know fun seven or

196
00:08:02,400 --> 00:08:06,840
anything else with a fun here it's

197
00:08:04,159 --> 00:08:09,919
function three which is kind of telling

198
00:08:06,840 --> 00:08:12,400
you with three bits what the actual

199
00:08:09,919 --> 00:08:14,879
function of the instruction is so trying

200
00:08:12,400 --> 00:08:17,879
to find where that comes from we can go

201
00:08:14,879 --> 00:08:20,879
back to our giant table of encodings

202
00:08:17,879 --> 00:08:23,000
chop it down to our ADDI and salty and

203
00:08:20,879 --> 00:08:25,120
salty U that we were looking at before

204
00:08:23,000 --> 00:08:27,840
and we can say these are the three bits

205
00:08:25,120 --> 00:08:30,039
that specify the fun three and so

206
00:08:27,840 --> 00:08:32,919
literally add I and salty they'll all

207
00:08:30,039 --> 00:08:36,560
have these seven bits the same but ADI

208
00:08:32,919 --> 00:08:40,880
will have 0 0 for fun 3 and salty will

209
00:08:36,560 --> 00:08:44,360
have 010 for fun 3 and so forth so this

210
00:08:40,880 --> 00:08:46,399
minor op code this function specified in

211
00:08:44,360 --> 00:08:48,480
three bits is doing the real heavy

212
00:08:46,399 --> 00:08:51,920
lifting of differentiating one assembly

213
00:08:48,480 --> 00:08:51,920
instruction from the others

