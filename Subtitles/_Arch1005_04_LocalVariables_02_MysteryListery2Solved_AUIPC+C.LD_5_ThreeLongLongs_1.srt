1
00:00:00,359 --> 00:00:04,480
so our investigation thus far has told

2
00:00:02,280 --> 00:00:06,120
us this is what's going on with two ins

3
00:00:04,480 --> 00:00:09,000
and this is what's going on with two

4
00:00:06,120 --> 00:00:11,880
Longs and that leaves us pondering what

5
00:00:09,000 --> 00:00:13,799
if we had three long Longs and that's an

6
00:00:11,880 --> 00:00:15,400
easy thing to test let's go ahead and

7
00:00:13,799 --> 00:00:17,320
queue up our three long Longs with

8
00:00:15,400 --> 00:00:20,960
leafless buold face affected oddballs

9
00:00:17,320 --> 00:00:22,880
and boted deadbeat compile that no new

10
00:00:20,960 --> 00:00:26,400
assembly instructions but three

11
00:00:22,880 --> 00:00:28,880
instances of that sequence o PC add I

12
00:00:26,400 --> 00:00:31,240
and load dword so of course these are

13
00:00:28,880 --> 00:00:33,719
going to be pulling in these 64-bit

14
00:00:31,240 --> 00:00:35,760
values via the load dword but what

15
00:00:33,719 --> 00:00:38,440
happens with the stack that's what you

16
00:00:35,760 --> 00:00:40,440
need to figure out go ahead and queue it

17
00:00:38,440 --> 00:00:43,559
up you know fill in your template if

18
00:00:40,440 --> 00:00:45,680
you'd like with this the starting values

19
00:00:43,559 --> 00:00:48,480
or just go ahead and jump into your

20
00:00:45,680 --> 00:00:50,079
debugger and start seeing where the

21
00:00:48,480 --> 00:00:51,800
stack starts when the code starts where

22
00:00:50,079 --> 00:00:53,280
the stack is manipulated where it is you

23
00:00:51,800 --> 00:00:55,000
know what's the subtraction from the

24
00:00:53,280 --> 00:00:57,520
starting point what are the values

25
00:00:55,000 --> 00:00:59,719
written where and go ahead and draw your

26
00:00:57,520 --> 00:01:01,840
stack diagram and then take a look at

27
00:00:59,719 --> 00:01:03,920
the two long Longs versus three long

28
00:01:01,840 --> 00:01:06,839
Longs and start to get a sense of what's

29
00:01:03,920 --> 00:01:06,839
going on

