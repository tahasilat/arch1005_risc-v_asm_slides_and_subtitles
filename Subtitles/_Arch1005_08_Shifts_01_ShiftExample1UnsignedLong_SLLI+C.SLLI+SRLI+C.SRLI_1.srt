1
00:00:00,160 --> 00:00:03,360
now we get to the portion of the class

2
00:00:01,680 --> 00:00:05,960
where we're going to collect an

3
00:00:03,360 --> 00:00:08,200
unreasonable number of shift assembly

4
00:00:05,960 --> 00:00:11,440
instructions so let's start with a

5
00:00:08,200 --> 00:00:15,599
simple example of an unsign long ABC set

6
00:00:11,440 --> 00:00:18,640
a to five shift a over by four bits to

7
00:00:15,599 --> 00:00:21,199
the left and set that equal to B and

8
00:00:18,640 --> 00:00:24,080
then shift B over by three bits to the

9
00:00:21,199 --> 00:00:26,119
right and set that equal to C what kind

10
00:00:24,080 --> 00:00:28,560
of assembly do we get from that well we

11
00:00:26,119 --> 00:00:31,640
get two new assembly instructions which

12
00:00:28,560 --> 00:00:37,079
I have decided to adopt and call Silly

13
00:00:31,640 --> 00:00:40,160
and sirly so silly shift left logical by

14
00:00:37,079 --> 00:00:42,640
immediate silly is actually two assembly

15
00:00:40,160 --> 00:00:44,879
instructions a 32-bit version and a

16
00:00:42,640 --> 00:00:48,079
64-bit version and while they're both

17
00:00:44,879 --> 00:00:50,840
called silly one of them shifts by five

18
00:00:48,079 --> 00:00:53,640
bits for 32-bit registers and one of

19
00:00:50,840 --> 00:00:56,079
them shifts by six bits for 64-bit

20
00:00:53,640 --> 00:01:00,399
registers and those values should make

21
00:00:56,079 --> 00:01:03,600
sense to you because 2 5 is 32 and 2 6

22
00:01:00,399 --> 00:01:06,479
is 64 therefore it doesn't make sense to

23
00:01:03,600 --> 00:01:09,479
shift the bits further to the left more

24
00:01:06,479 --> 00:01:11,680
than 32 bits for 32-bit register or I

25
00:01:09,479 --> 00:01:14,840
should have said 31 bits for a 32-bit

26
00:01:11,680 --> 00:01:16,720
register or 63 bits for a 64-bit

27
00:01:14,840 --> 00:01:20,720
register because of course they en code

28
00:01:16,720 --> 00:01:22,479
0 to 31 and 0 to 63 so the silly

29
00:01:20,720 --> 00:01:25,680
instruction just takes the source

30
00:01:22,479 --> 00:01:28,200
register and shifts it left by however

31
00:01:25,680 --> 00:01:30,280
many bits is specified by these five

32
00:01:28,200 --> 00:01:32,600
bits it's not shifting by five bits it's

33
00:01:30,280 --> 00:01:35,399
using five bits to encode the values 0

34
00:01:32,600 --> 00:01:38,040
through 31 so shift it left and store

35
00:01:35,399 --> 00:01:41,200
that result in the destination register

36
00:01:38,040 --> 00:01:44,240
now this is a logical shift in a logical

37
00:01:41,200 --> 00:01:47,119
shift you fill in the shifted bits with

38
00:01:44,240 --> 00:01:49,320
zeros we'll show that in a second and

39
00:01:47,119 --> 00:01:51,479
this is a left shift meaning that it's

40
00:01:49,320 --> 00:01:54,079
going to be those least significant bits

41
00:01:51,479 --> 00:01:55,719
as we shift over to the left so let's

42
00:01:54,079 --> 00:01:59,520
draw a picture of that let's assume that

43
00:01:55,719 --> 00:02:02,119
we had silly of a4 a512 that's going to

44
00:01:59,520 --> 00:02:04,880
take the the a5 register shift it left

45
00:02:02,119 --> 00:02:07,159
logically by 12 bits and put the result

46
00:02:04,880 --> 00:02:09,479
into a4 let's assume that when this

47
00:02:07,159 --> 00:02:12,040
assembly instruction is about to be run

48
00:02:09,479 --> 00:02:14,400
we have a5 holding staba daaba dudes

49
00:02:12,040 --> 00:02:17,160
again and we are then going to shift it

50
00:02:14,400 --> 00:02:20,200
left by 12 and that yields the following

51
00:02:17,160 --> 00:02:22,360
result 12 bits at the bottom are set to

52
00:02:20,200 --> 00:02:25,239
zero these bits from the least

53
00:02:22,360 --> 00:02:29,000
significant bits are shifted up by 12

54
00:02:25,239 --> 00:02:31,280
bits so what was bit zero becomes bit 12

55
00:02:29,000 --> 00:02:34,120
and the bottom bit 0 through 11 are now

56
00:02:31,280 --> 00:02:35,879
all zeros and these upper 12 bits just

57
00:02:34,120 --> 00:02:38,640
kind of fall out of the side of the

58
00:02:35,879 --> 00:02:42,080
register and get lost to history so the

59
00:02:38,640 --> 00:02:44,879
net result is that we have this bad bad

60
00:02:42,080 --> 00:02:47,000
dudes or whatever it is anyways just 12

61
00:02:44,879 --> 00:02:49,080
bits of zeros show up at the bottom and

62
00:02:47,000 --> 00:02:51,239
everything else is shifted to the left

63
00:02:49,080 --> 00:02:54,640
and you lose the top 12 bits that's

64
00:02:51,239 --> 00:02:57,519
shift left logical by an immediate shift

65
00:02:54,640 --> 00:02:59,080
right logical by an immediate or Surly

66
00:02:57,519 --> 00:03:01,080
is the same thing just going the

67
00:02:59,080 --> 00:03:03,159
opposite direction you have a source and

68
00:03:01,080 --> 00:03:06,519
if it's 32bit you're going to shift it

69
00:03:03,159 --> 00:03:11,280
by 0 to 31 and if it's 64 you shift it

70
00:03:06,519 --> 00:03:12,920
to the right by 0 to 63 and so yeah just

71
00:03:11,280 --> 00:03:15,640
everything is the same it's a logical

72
00:03:12,920 --> 00:03:17,519
shift so it is always filling it in with

73
00:03:15,640 --> 00:03:19,720
zeros and because this is Shifting to

74
00:03:17,519 --> 00:03:21,360
the right the filling happens at the

75
00:03:19,720 --> 00:03:25,599
most significant bits rather than the

76
00:03:21,360 --> 00:03:28,680
least significant so Surly a4 a512 a5

77
00:03:25,599 --> 00:03:31,159
once again staba daa dudes shift right

78
00:03:28,680 --> 00:03:34,319
by 12 and you get this the most

79
00:03:31,159 --> 00:03:37,040
significant bits are shifted down by 12

80
00:03:34,319 --> 00:03:40,360
and the upper 12 bits are filled in with

81
00:03:37,040 --> 00:03:43,080
zeros always zeros for a logical shift

82
00:03:40,360 --> 00:03:46,439
okay well that was shift left and shift

83
00:03:43,080 --> 00:03:49,280
right logical by immediate sirly and

84
00:03:46,439 --> 00:03:51,040
silly go ahead and stop and step through

85
00:03:49,280 --> 00:03:53,159
the assembly and make sure you

86
00:03:51,040 --> 00:03:56,599
understand how it corresponds to the C

87
00:03:53,159 --> 00:03:56,599
source code that you were given

