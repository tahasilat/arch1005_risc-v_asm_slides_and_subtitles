1
00:00:00,520 --> 00:00:05,680
okay so if everything went according to

2
00:00:02,280 --> 00:00:09,519
plan and you stepped through the line

3
00:00:05,680 --> 00:00:11,599
5642 or main plus 26 you would see

4
00:00:09,519 --> 00:00:13,280
something like this Frame pointer of

5
00:00:11,599 --> 00:00:15,759
course moved down again pointing at the

6
00:00:13,280 --> 00:00:18,039
stuff we don't care saved frame pointer

7
00:00:15,759 --> 00:00:20,119
points back to where the previous frame

8
00:00:18,039 --> 00:00:21,680
pointer pointed which is this address up

9
00:00:20,119 --> 00:00:24,960
here F

10
00:00:21,680 --> 00:00:29,359
aa8 and we still have uninitialized data

11
00:00:24,960 --> 00:00:31,560
at 910 and 900 but now we have obsolete

12
00:00:29,359 --> 00:00:33,360
at the upper 32 bits where scalable

13
00:00:31,560 --> 00:00:35,640
previously was in the single local

14
00:00:33,360 --> 00:00:38,520
variable and we have scalable moved down

15
00:00:35,640 --> 00:00:43,719
to the lower 32 bits so scalable is at

16
00:00:38,520 --> 00:00:45,840
9008 and obsolete is at 900c the 8 + 4

17
00:00:43,719 --> 00:00:48,199
so that's our takeaway from this it's

18
00:00:45,840 --> 00:00:50,120
the fact that obsolete is placed where

19
00:00:48,199 --> 00:00:51,760
scalable is and scalable seems to have

20
00:00:50,120 --> 00:00:54,239
moved down but we still have this

21
00:00:51,760 --> 00:00:55,879
uninitialized space so we're still not

22
00:00:54,239 --> 00:00:57,280
exactly sure what's going on with this

23
00:00:55,879 --> 00:00:59,160
un initialized space or that

24
00:00:57,280 --> 00:01:01,640
uninitialized space so we're going to

25
00:00:59,160 --> 00:01:05,600
need to continue the inference game and

26
00:01:01,640 --> 00:01:05,600
try some more examples

