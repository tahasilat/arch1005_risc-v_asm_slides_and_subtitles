1
00:00:00,280 --> 00:00:05,799
okay so we've started learning about the

2
00:00:01,959 --> 00:00:07,640
RV 32m and RV 64m assembly instructions

3
00:00:05,799 --> 00:00:10,360
but we don't care as usual about the

4
00:00:07,640 --> 00:00:12,719
actual instruction encoding we just want

5
00:00:10,360 --> 00:00:15,200
a checklist to keep track of things and

6
00:00:12,719 --> 00:00:19,720
again as a reminder the RV 64m just like

7
00:00:15,200 --> 00:00:23,039
RV64I the extensions include the RV 32m

8
00:00:19,720 --> 00:00:26,439
versions so we learned about the RV 32m

9
00:00:23,039 --> 00:00:29,080
mle which keeps only the lower half this

10
00:00:26,439 --> 00:00:31,119
H version of mul right here these three

11
00:00:29,080 --> 00:00:33,440
variants is going to be the versions

12
00:00:31,119 --> 00:00:36,200
that keep the upper half we learned

13
00:00:33,440 --> 00:00:38,160
about D view the unsigned operand

14
00:00:36,200 --> 00:00:39,640
version of division and of course we

15
00:00:38,160 --> 00:00:43,120
have a div here which is probably going

16
00:00:39,640 --> 00:00:45,800
to be implicitly signed and remu the

17
00:00:43,120 --> 00:00:47,000
remainder unsigned operands and the REM

18
00:00:45,800 --> 00:00:49,360
is probably going to be the signed

19
00:00:47,000 --> 00:00:51,399
version as well so our takeaways from

20
00:00:49,360 --> 00:00:54,120
example one is that if we have a

21
00:00:51,399 --> 00:00:57,280
multiplication or division that is not a

22
00:00:54,120 --> 00:00:59,519
power of two then the if the compiler

23
00:00:57,280 --> 00:01:01,559
has support for the M extension the

24
00:00:59,519 --> 00:01:03,719
multip ation extensions then it'll

25
00:01:01,559 --> 00:01:06,560
generate multiplication division or

26
00:01:03,719 --> 00:01:08,920
remainder operations uh using new

27
00:01:06,560 --> 00:01:10,600
assembly instructions we saw previously

28
00:01:08,920 --> 00:01:12,119
when the multiplication or division was

29
00:01:10,600 --> 00:01:14,240
by a power of two it would just use

30
00:01:12,119 --> 00:01:16,360
shifts because that's far more efficient

31
00:01:14,240 --> 00:01:18,560
we also saw our first multiply which

32
00:01:16,360 --> 00:01:20,119
keeps only the lower bits perhaps we

33
00:01:18,560 --> 00:01:22,240
would want to see a multiply that keeps

34
00:01:20,119 --> 00:01:23,799
the lower and upper bits but as we can

35
00:01:22,240 --> 00:01:26,119
see we stored the result to the

36
00:01:23,799 --> 00:01:28,680
multiplication back into an unsigned

37
00:01:26,119 --> 00:01:30,680
64-bit integer and that means whatever

38
00:01:28,680 --> 00:01:33,159
the result it's going to just throw away

39
00:01:30,680 --> 00:01:35,399
the top 64 bits and keep only the bottom

40
00:01:33,159 --> 00:01:40,000
64 bits and that's why we only saw

41
00:01:35,399 --> 00:01:40,000
multiply that keeps the lower 64 bits

