1
00:00:00,320 --> 00:00:05,520
so the takeaway from example six is that

2
00:00:02,919 --> 00:00:07,680
the M Hsu should be used when you want

3
00:00:05,520 --> 00:00:10,280
to save the high bits and you want to

4
00:00:07,680 --> 00:00:13,040
multiply a signed value by an unsigned

5
00:00:10,280 --> 00:00:15,160
value and so that's what we got from

6
00:00:13,040 --> 00:00:17,359
this code if you walked through it all

7
00:00:15,160 --> 00:00:20,439
you saw that actually there was a little

8
00:00:17,359 --> 00:00:22,760
silly shift left logical by immediate in

9
00:00:20,439 --> 00:00:25,080
here and the immediate is one so

10
00:00:22,760 --> 00:00:27,519
shifting left by one is like multiplying

11
00:00:25,080 --> 00:00:30,080
by 2 to the 1 which is multiplying by

12
00:00:27,519 --> 00:00:31,599
two and why does that make sense because

13
00:00:30,080 --> 00:00:34,879
you had as part of the final

14
00:00:31,599 --> 00:00:37,760
multiplication a 128 times itself which

15
00:00:34,879 --> 00:00:39,920
is basically multiplying Itself by two

16
00:00:37,760 --> 00:00:43,200
and with that you've done it again

17
00:00:39,920 --> 00:00:43,200
you've collected all the

18
00:00:44,550 --> 00:00:50,000
[Music]

19
00:00:47,000 --> 00:00:50,000
stars

